package com.androidbegin.practica04;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import com.androidbegin.fragmenttabstutorial.R;

import android.app.ActionBar;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;


public class MainActivity extends Activity {
	
	public final static String EXTRA_MESSAGE = "com.androidbegin.fragmenttabstutorial";
	Button save,load;
	EditText message;
	String Message;
	int data_block = 100;
	@Override

	 protected void onCreate(Bundle savedInstanceState) {

	 super.onCreate(savedInstanceState);
	 setContentView(R.layout.activity_main);
	 save = (Button)findViewById(R.id.SAVE);
	 load = (Button)findViewById(R.id.Load);
	 message = (EditText)findViewById(R.id.msg);
	 save.setOnClickListener(new View.OnClickListener() {
	 
	@Override

	 public void onClick(View view) {
	 Message = message.getText().toString();
			 try {
				 FileOutputStream fou = openFileOutput("text.txt",MODE_WORLD_READABLE);
				 OutputStreamWriter osw = new OutputStreamWriter(fou);
			 try {
				 osw.write(Message);
				 osw.flush();	
				 osw.close();	
				 Toast.makeText(getBaseContext(),"Dato guardado",Toast.LENGTH_LONG).show();

				 } catch (IOException e) {
					 e.printStackTrace();
				 }

				 } catch (FileNotFoundException e) {
				 e.printStackTrace();
				 }
		}
	 });
	 load.setOnClickListener(new View.OnClickListener() {

		 @Override

		 public void onClick(View view) {

		 try {

			 FileInputStream fis = openFileInput("text.txt");
			 InputStreamReader isr = new InputStreamReader(fis);
			 char[] data = new char[data_block];
			 String final_data ="";
			 int size;
			 try {

				 while((size = isr.read(data))>0){
				 String read_data = String.valueOf(data,0,size);
				 final_data += read_data;
				 data = new char[data_block];
				 }

				 Toast.makeText(getBaseContext(),"Message:"+final_data,Toast.LENGTH_LONG).show();

			 } catch (IOException e) {
				 e.printStackTrace();
			 	}

		 } catch (FileNotFoundException e) {
		 e.printStackTrace();
		 }

		 }

		 });
	}
	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
        	
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	 // Handle presses on the action bar items
    	 switch (item.getItemId()) {
    	 case R.id.action_search:
    	 openSearch();
    	 return true;
    	 case R.id.menu_settings:
    	 openSettings();
    	 return true;
    	 default:
    	 return super.onOptionsItemSelected(item);
    	 }

    }
    
    private void openSearch() {
        Toast.makeText(this, "Search button pressed", Toast.LENGTH_SHORT).show();
    }
    
    private void openSettings() {
        Toast.makeText(this, "Settings button pressed", Toast.LENGTH_SHORT).show();
    }
    
   
	 
}
